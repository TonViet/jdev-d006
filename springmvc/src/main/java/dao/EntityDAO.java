package dao;

import java.util.List;

import com.website.springmvc.entity.Student;

public abstract class EntityDAO<T> {
	
	public abstract List<T> getAll();	
	public abstract T get(long id);
	public abstract Student insert(T t) throws Exception;
	public abstract void update(T t) throws Exception;
	public abstract void delete(T t) throws Exception;
}
